@isTest
public class MyTest {
    static TestHelper theHelper;
    static Account_Holder__c theAccountHolder1;
    static Account_Holder__c theAccountHolder2;
    
    static void setupData(){
        
        theHelper = new TestHelper('uber.com');
        theHelper.createTestDataForCaseAndContact();
       
        
        List<Account_Holder__c> accountHoldersToCreate = new List<Account_Holder__c>();
       
        theAccountHolder1 = new Account_Holder__c();
        theAccountHolder1.Request__c = theHelper.testCase.Id;
        theAccountHolder1.Actionable__c = true;
        theAccountHolder1.Count__c = 2;
        theAccountHolder1.Relationship__c = 'Rider';
        accountHoldersToCreate.add(theAccountHolder1);
        
        theAccountHolder2 = new Account_Holder__c();
        theAccountHolder2.Request__c = theHelper.testCase.Id;
        theAccountHolder2.Actionable__c = true;
        theAccountHolder2.Count__c = 2;
        theAccountHolder2.Relationship__c = 'Driver';
        accountHoldersToCreate.add(theAccountHolder2);
        
        insert accountHoldersToCreate;
    }
    
    @IsTest
    static void testGetAllAccountHolders(){
        setupData();
        AccountHolderController.AccountHolderDetailsWrapper theDetail;
        Test.startTest();
            theDetail = AccountHolderController.getAllAccountHolders(theHelper.testCase.Id);
        Test.stopTest();
        System.assertEquals(2, theDetail.accountHolders.size());
        
        
    }
    
    @IsTest
    static void testGetBusinessUnitToTypeMapping(){
        setupData();
        List<String> availableTypes;
        Test.startTest();
            availableTypes = AccountHolderController.getBusinessUnitToTypeMapping('Rides');
        Test.stopTest();
        System.assert(availableTypes.size() > 0);
    }
    
    @IsTest
    static void testSaveAccountHolder(){
        setupData();
        
        Test.startTest();
            AccountHolderController.saveAccountHolder('{"actionable":true,"accountProduced":true,"count":"2","type":"Third Party","id":""}' ,theHelper.testCase.Id, theAccountHolder1.Id);
        Test.stopTest();
        List<Account_Holder__c> queriedAccountHolders = [SELECT Id FROM Account_Holder__c];
        System.assertEquals(2, queriedAccountHolders.size() , '2 account holders should have been returned.1 was added.1 was deleted.So , 2 remained.');
        
    }
    
    static testmethod void testDeleteAccountHolder(){
        setupData();
        
        Test.startTest();
            AccountHolderController.deleteAccountHolder(theAccountHolder1.Id);
        Test.stopTest();
        List<Account_Holder__c> queriedAccountHolders = [SELECT Id FROM Account_Holder__c];
        System.assertEquals(1, queriedAccountHolders.size() , '1 account holder should have been returned.');
        
    }
    
    @IsTest
    static void testGetBusinessUnit(){
        setupData();
        
        String businessUnit = '';
        Test.startTest();
            businessUnit = AccountHolderController.getBusinessUnit(theHelper.testCase.Id);
        Test.stopTest();
       
        System.assertEquals('Rides', businessUnit , 'Rides should have been returned.');
    }
    
    
}