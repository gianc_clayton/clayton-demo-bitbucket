<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>BME - A Use Case represents a specific business scenario. Multiple Use Cases can exist for a Work Package and multiple requirements can be supported by Use Case. With each Use Case the user can create Use Case Steps that represent the incremental actions that, when joined in sequence, make up a Use Case.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Actor__c</fullName>
        <externalId>false</externalId>
        <label>Actor(s)</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>3rd Party Prequal Team</fullName>
                    <default>false</default>
                    <label>3rd Party Prequal Team</label>
                </value>
                <value>
                    <fullName>Account Compliance Team</fullName>
                    <default>false</default>
                    <label>Account Compliance Team</label>
                </value>
                <value>
                    <fullName>Account Setup Team</fullName>
                    <default>false</default>
                    <label>Account Setup Team</label>
                </value>
                <value>
                    <fullName>Billing Admin</fullName>
                    <default>false</default>
                    <label>Billing Admin</label>
                </value>
                <value>
                    <fullName>Billing System</fullName>
                    <default>false</default>
                    <label>Billing System</label>
                </value>
                <value>
                    <fullName>BRM</fullName>
                    <default>false</default>
                    <label>BRM</label>
                </value>
                <value>
                    <fullName>Change Manager</fullName>
                    <default>false</default>
                    <label>Change Manager</label>
                </value>
                <value>
                    <fullName>CRM PeopleSoft</fullName>
                    <default>false</default>
                    <label>CRM PeopleSoft</label>
                </value>
                <value>
                    <fullName>CSM</fullName>
                    <default>false</default>
                    <label>CSM</label>
                </value>
                <value>
                    <fullName>CSR</fullName>
                    <default>false</default>
                    <label>CSR</label>
                </value>
                <value>
                    <fullName>Customer Gateway</fullName>
                    <default>false</default>
                    <label>Customer Gateway</label>
                </value>
                <value>
                    <fullName>Data Integration</fullName>
                    <default>false</default>
                    <label>Data Integration</label>
                </value>
                <value>
                    <fullName>DSS/POP</fullName>
                    <default>false</default>
                    <label>DSS/POP</label>
                </value>
                <value>
                    <fullName>Implementer</fullName>
                    <default>false</default>
                    <label>Implementer</label>
                </value>
                <value>
                    <fullName>Installation Group</fullName>
                    <default>false</default>
                    <label>Installation Group</label>
                </value>
                <value>
                    <fullName>Installation Portal</fullName>
                    <default>false</default>
                    <label>Installation Portal</label>
                </value>
                <value>
                    <fullName>ISR</fullName>
                    <default>false</default>
                    <label>ISR</label>
                </value>
                <value>
                    <fullName>Marketing Dept</fullName>
                    <default>false</default>
                    <label>Marketing Dept</label>
                </value>
                <value>
                    <fullName>NMC User</fullName>
                    <default>false</default>
                    <label>NMC User</label>
                </value>
                <value>
                    <fullName>NMP</fullName>
                    <default>false</default>
                    <label>NMP</label>
                </value>
                <value>
                    <fullName>Operations Engineer</fullName>
                    <default>false</default>
                    <label>Operations Engineer</label>
                </value>
                <value>
                    <fullName>Operations Manager</fullName>
                    <default>false</default>
                    <label>Operations Manager</label>
                </value>
                <value>
                    <fullName>Order Admin</fullName>
                    <default>false</default>
                    <label>Order Admin</label>
                </value>
                <value>
                    <fullName>PM</fullName>
                    <default>false</default>
                    <label>PM</label>
                </value>
                <value>
                    <fullName>Requester</fullName>
                    <default>false</default>
                    <label>Requester</label>
                </value>
                <value>
                    <fullName>Salesforce (SFDC)</fullName>
                    <default>false</default>
                    <label>Salesforce (SFDC)</label>
                </value>
                <value>
                    <fullName>SBSS Support</fullName>
                    <default>false</default>
                    <label>SBSS Support</label>
                </value>
                <value>
                    <fullName>Service Cloud</fullName>
                    <default>false</default>
                    <label>Service Cloud</label>
                </value>
                <value>
                    <fullName>Service Delivery System</fullName>
                    <default>false</default>
                    <label>Service Delivery System</label>
                </value>
                <value>
                    <fullName>Tech Support Member</fullName>
                    <default>false</default>
                    <label>Tech Support Member</label>
                </value>
                <value>
                    <fullName>Vantive</fullName>
                    <default>false</default>
                    <label>Vantive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Assumptions__c</fullName>
        <externalId>false</externalId>
        <label>Assumptions</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Description of scenario/use case</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Next_Steps__c</fullName>
        <externalId>false</externalId>
        <label>Next Steps</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Post_Condition__c</fullName>
        <externalId>false</externalId>
        <label>Post-Conditions</label>
        <length>32000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Pre_condition__c</fullName>
        <externalId>false</externalId>
        <label>Pre-Conditions</label>
        <length>32000</length>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Process_Dependencies__c</fullName>
        <externalId>false</externalId>
        <label>Process Dependencies</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Related_Requirements__c</fullName>
        <externalId>false</externalId>
        <label>Related Requirements</label>
        <length>32768</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Related_Use_Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Related Use Case</label>
        <referenceTo>Use_Case__c</referenceTo>
        <relationshipLabel>Related Use Cases</relationshipLabel>
        <relationshipName>UseCases</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Release</label>
        <referenceTo>Release__c</referenceTo>
        <relationshipLabel>Use Cases</relationshipLabel>
        <relationshipName>Use_Cases_del</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Signed_off_on__c</fullName>
        <externalId>false</externalId>
        <label>Signed off on</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Started</fullName>
                    <default>false</default>
                    <label>Not Started</label>
                </value>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Reviewed Internally</fullName>
                    <default>false</default>
                    <label>Reviewed Internally</label>
                </value>
                <value>
                    <fullName>Reviewed with Hughes</fullName>
                    <default>false</default>
                    <label>Reviewed with Hughes</label>
                </value>
                <value>
                    <fullName>Waiting for Sign-off</fullName>
                    <default>false</default>
                    <label>Waiting for Sign-off</label>
                </value>
                <value>
                    <fullName>Sign-off Received</fullName>
                    <default>false</default>
                    <label>Sign-off Received</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>System_Touch_Points__c</fullName>
        <externalId>false</externalId>
        <label>System Touch Points</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Point1</fullName>
                    <default>false</default>
                    <label>Point1</label>
                </value>
                <value>
                    <fullName>Point2</fullName>
                    <default>false</default>
                    <label>Point2</label>
                </value>
                <value>
                    <fullName>Point3</fullName>
                    <default>false</default>
                    <label>Point3</label>
                </value>
                <value>
                    <fullName>Point4</fullName>
                    <default>false</default>
                    <label>Point4</label>
                </value>
                <value>
                    <fullName>Point5</fullName>
                    <default>false</default>
                    <label>Point5</label>
                </value>
                <value>
                    <fullName>Point6</fullName>
                    <default>false</default>
                    <label>Point6</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Trigger__c</fullName>
        <externalId>false</externalId>
        <label>Trigger</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Use_Case_Name__c</fullName>
        <externalId>false</externalId>
        <label>Use Case Name</label>
        <length>80</length>
        <required>true</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Work_Package_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Work_Package__r.Work_Package_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Work Package Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Work_Package__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Work Package</label>
        <referenceTo>Work_Package__c</referenceTo>
        <relationshipLabel>Use Cases</relationshipLabel>
        <relationshipName>Use_Cases</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Use Case</label>
    <listViews>
        <fullName>AllUseCases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Actor__c</columns>
        <columns>Trigger__c</columns>
        <columns>Release__c</columns>
        <columns>Status__c</columns>
        <columns>Work_Package__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All Use Cases</label>
    </listViews>
    <listViews>
        <fullName>ECOB_Use_Cases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Actor__c</columns>
        <columns>Trigger__c</columns>
        <columns>Description__c</columns>
        <columns>Pre_condition__c</columns>
        <columns>Post_Condition__c</columns>
        <columns>Work_Package_Name__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release__c</field>
            <operation>equals</operation>
            <value>ECOB</value>
        </filters>
        <filters>
            <field>Work_Package__c</field>
            <operation>equals</operation>
            <value>WP-1746</value>
        </filters>
        <label>ECOB Use Cases</label>
    </listViews>
    <listViews>
        <fullName>My_Use_Cases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Actor__c</columns>
        <columns>Trigger__c</columns>
        <columns>Work_Package__c</columns>
        <filterScope>Mine</filterScope>
        <label>My Use Cases</label>
    </listViews>
    <listViews>
        <fullName>R1UseCases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Actor__c</columns>
        <columns>Trigger__c</columns>
        <columns>Work_Package__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release__c</field>
            <operation>contains</operation>
            <value>1</value>
        </filters>
        <label>R1 Use Cases</label>
    </listViews>
    <listViews>
        <fullName>R2UseCases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Description__c</columns>
        <columns>Release__c</columns>
        <columns>Status__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>LAST_UPDATE</columns>
        <columns>LAST_ACTIVITY</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release__c</field>
            <operation>contains</operation>
            <value>R2</value>
        </filters>
        <label>R2 Use Cases</label>
    </listViews>
    <listViews>
        <fullName>R3UseCases</fullName>
        <columns>NAME</columns>
        <columns>Use_Case_Name__c</columns>
        <columns>Work_Package__c</columns>
        <columns>Description__c</columns>
        <columns>Trigger__c</columns>
        <columns>Status__c</columns>
        <columns>Signed_off_on__c</columns>
        <columns>Actor__c</columns>
        <columns>Next_Steps__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>UPDATEDBY_USER</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release__c</field>
            <operation>contains</operation>
            <value>R3</value>
        </filters>
        <label>R3 Use Cases</label>
    </listViews>
    <nameField>
        <displayFormat>UC-{00000}</displayFormat>
        <label>Use Case ID</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Use Cases</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Use_Case_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Actor__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Trigger__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Work_Package__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Use_Case_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Actor__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Trigger__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Work_Package__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Use_Case_Name__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Actor__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Trigger__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Work_Package__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Use_Case_Name__c</searchFilterFields>
        <searchFilterFields>Actor__c</searchFilterFields>
        <searchFilterFields>Trigger__c</searchFilterFields>
        <searchFilterFields>Work_Package__c</searchFilterFields>
        <searchResultsAdditionalFields>Use_Case_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Actor__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Trigger__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Work_Package__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Add_Update_Steps_s</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Add/ Update Steps(s)</masterLabel>
        <openType>sidebar</openType>
        <page>EditUseCase</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>